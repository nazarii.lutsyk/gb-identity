import { Identifier } from './Identifier';

export abstract class Entity<T> {
	protected readonly _id: Identifier;
	protected readonly props: T;


	constructor(id: Identifier, props: T) {
		this._id = id;
		this.props = props;
	}

	public equals(object?: Entity<T>): boolean {
		if (object === null || object === undefined) {
			return false;
		}

		if (this === object) {
			return true;
		}

		if (!isEntity(object)) {
			return false;
		}

		return this._id.equals(object._id);
	}
}

const isEntity = (v: unknown): v is Entity<unknown> => {
	return v instanceof Entity;
};

import convict from 'convict';
import env from 'dotenv';
import path from 'path';

env.config({ path: path.join(__dirname, '../../.env') });

const config = convict({
	env: {
		doc: 'The application environment',
		format: ['local', 'production'],
		default: 'local',
		env: 'NODE_ENV',
	},
	server: {
		port: {
			doc: 'The http server port',
			format: 'port',
			default: '3000',
			env: 'HTTP_SERVER_PORT',
		},
	},
	db: {
		host: {
			doc: 'Database connection host',
			format: String,
			default: '127.0.0.1',
			env: 'DB_CONNECTION_HOST',
		},
		port: {
			doc: 'Database connection port',
			format: Number,
			default: '3306',
			env: 'DB_CONNECTION_PORT',
		},
		user: {
			doc: 'Database connection user',
			format: String,
			default: '',
			env: 'DB_CONNECTION_USER',
		},
		password: {
			doc: 'Database connection password',
			format: String,
			default: '',
			env: 'DB_CONNECTION_PASSWORD',
		},
		database: {
			doc: 'Database name',
			format: String,
			default: 'Identity',
			env: 'DB_NAME',
		},
	},
	google: {
		clientId: {
			doc: 'Google authentication client id',
			format: String,
			env: 'GOOGLE_CLIENT_ID',
			default: '',
		},
		clientSecret: {
			doc: 'Google authentication client secret',
			format: String,
			env: 'GOOGLE_CLIENT_SECRET',
			default: '',
		},
		clientRedirectUrl: {
			doc: 'Google authentication client redirect url',
			format: String,
			env: 'GOOGLE_CLIENT_REDIRECT_URL',
			default: '',
		},
	},
	tokens: {
		access: {
			expiration: {
				doc: 'Access token expiration',
				format: String,
				env: 'ACCESS_TOKEN_EXPIRATION',
				default: '1m',
			},
		},
		refresh: {
			expiration: {
				doc: 'Refresh token expiration',
				format: String,
				env: 'REFRESH_TOKEN_EXPIRATION',
				default: '5m',
			},
		},
		publicKey: {
			doc: 'Public key',
			format: String,
			env: 'TOKEN_PUBLIC_KEY',
			default: '1111',
		},
		privateKey: {
			doc: 'Public key',
			format: String,
			env: 'TOKEN_PRIVATE_KEY',
			default: '0000',
		},
	},
});

export default config;

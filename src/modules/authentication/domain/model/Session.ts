import { AggregateRoot } from '../../../../shared/domain/AggregateRoot';
import { SessionId } from './SessionId';
import { UserId } from '../../../users/domain/model/UserId';
import { Validator } from '../../../../shared/libs/Validator';
import { Either, right, wrong } from '../../../../shared/core/Result';
import { PropertyRequiredError, ValidationError } from '../../../../shared/core/BusinessError';
import { Token, TokenPair, TokenType } from './Token';

type SessionProps = {
  userId: UserId;
  tokenPair?: TokenPair;
};

export class Session extends AggregateRoot<SessionProps> {
  get id(): SessionId {
    return this._id;
  }

  get userId(): UserId {
    return this.props.userId;
  }

  static create(id: SessionId, props: SessionProps): Either<ValidationError, Session> {
    const validatorResult = Validator.againstNullOrUndefinedBulk(
      { key: 'userId', value: props.userId }
    );
    if (validatorResult.isWrong()) {
      return wrong(new PropertyRequiredError(validatorResult.value.properties));
    }

    return right(new Session(id, props));
  }

  public assignTokenPair(accessToken: Token, refreshToken: Token): void {
    this.props.tokenPair = new Map<TokenType, Token>();
    this.props.tokenPair.set(TokenType.ACCESS, accessToken);
    this.props.tokenPair.set(TokenType.REFRESH, refreshToken);
  }

  public tokenPair(): TokenPair | undefined {
    return this.props.tokenPair;
  }
}

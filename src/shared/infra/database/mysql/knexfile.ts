import { Knex } from 'knex';
import config from '../../../../config/config';
import Log from '../../../libs/Logger';

const knexConfig: Knex.Config = {
	debug: config.get('env') === 'local',
	client: 'mysql2',
	connection: {
		host: config.get('db.host'),
		port: Number(config.get('db.port')),
		user: config.get('db.user'),
		password: config.get('db.password'),
		database: config.get('db.database'),
	},
	migrations: {
		directory: './migrations',
		tableName: 'Migration'
	},
	log: {
		debug(message: unknown) {
			Log.debug(message);
		},
		warn(message: unknown) {
			Log.warn(message);
		},
		error(message: unknown) {
			Log.error(message);
		},
		deprecate(method: string, alternative: string) {
			Log.warn(`${method} is deprecated please use ${alternative} instead`);
		},
	},
};

export default knexConfig;

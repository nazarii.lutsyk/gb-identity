import { ValueObject } from '../../../../shared/domain/ValueObject';
import { Either, right, wrong } from '../../../../shared/core/Result';
import { PropertyInvalidError } from '../../../../shared/core/BusinessError';

type TokenStringProps = {
	token: string;
};

export class TokenString extends ValueObject<TokenStringProps> {
	get token(): string {
		return this.props.token;
	}

	private static isValidToken(token: string) {
		const re = /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/;
		return re.test(token);
	}

	public static create(token: string): Either<PropertyInvalidError, TokenString> {
		if (!TokenString.isValidToken(token)) {
			return wrong(new PropertyInvalidError(['token'], 'does not comply with regex'));
		} else {
			return right(new TokenString({ token }));
		}
	}
}

import { BusinessError } from '../../../../shared/core/BusinessError';
import { Either } from '../../../../shared/core/Result';
import { Session } from '../model/Session';
import { SessionId } from '../model/SessionId';

export interface SessionRepository {
    nextIdentifier(): SessionId;
    saveSession(session: Session): Promise<Either<BusinessError, SessionId>>;
    deleteSessionById(sessionId: SessionId): Promise<Either<BusinessError, null>>;
    sessionExistsById(sessionId: SessionId): Promise<Either<BusinessError, boolean>>;
    sessionById(sessionId: SessionId): Promise<Either<BusinessError, Session>>;
}

import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema
        .createTable('User', table => {
            table.string('id').primary().notNullable().unique();
            table.string('email').notNullable();
        })
        .createTable('Session', table => {
            table.string('id').primary().notNullable().unique();
            table.string('user_id').references('id').inTable('User').notNullable();
        });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('User')
    .dropTableIfExists('Session');
}

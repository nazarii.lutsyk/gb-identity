import { Principal } from '../../src/shared/domain/Principal';

declare global {
    declare namespace Express {
        export interface Request {
            principal: Principal;
        }
    }
}

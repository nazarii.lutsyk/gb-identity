export namespace GoogleAccountRepresentation {
	export interface Response {
		config: Config;
		data: Data;
		headers: Headers;
		status: number;
		statusText: string;
		request: {
			responseURL: string;
		};
	}

	interface Config {
		url: string;
		method: string;
		userAgentDirectives: {
			product: string;
			version: string;
			comment: string;
		}[];
		headers: Headers;
		params: {
			personFields: string;
		};
		retry: boolean;
		responseType: string;
	}

	type Headers = {
		[keyof in string]: string;
	};


	interface Source {
		type: string;
		id: string;
	}

	interface Name {
		metadata: {
			primary: boolean;
			source: Source;
		};
		displayName: string;
		familyName: string;
		givenName: string;
		displayNameLastFirst: string;
		unstructuredName: string;
	}

	interface Photo {
		metadata: {
			primary: boolean;
			source: Source;
		};
		url: string;
	}

	interface EmailAddress {
		metadata: {
			primary: boolean;
			verified: boolean;
			source: Source;
			sourcePrimary: boolean;
		};
		value: string;
	}

	interface Data {
		resourceName: string;
		etag: string;
		names: Name[];
		photos: Photo[];
		emailAddresses: EmailAddress[];
	}

}

import { NextFunction, Request, Response } from 'express';
import { Principal } from '../../../../../../shared/domain/Principal';
import { BaseController } from '../../../../../../shared/infra/api/controllers/BaseController';
import { VerifySessionUseCase } from '../../../../application/verify-session/VerifySessionUseCase';

// todo move to gateway service
export class VerifySessionMiddleware extends BaseController {
    verifySessionUseCase: VerifySessionUseCase;

    constructor(verifySessionUseCase: VerifySessionUseCase) {
        super();
        this.verifySessionUseCase = verifySessionUseCase;
    }

    protected async handle(req: Request, res: Response, next: NextFunction): Promise<unknown> {
        const token = req.headers['authorization'] as string;
        if (!token) {
            return this.badRequest(res, 'token is required');
        }

        const result = await this.verifySessionUseCase.execute(token);
		if (result.isWrong()) {
			return this.forbidden(res, result.value.message);
		}

        const principalResult = Principal.create({
            sessionId: result.value.sessionId.value,
            userId: result.value.userId.value,
            type: result.value.type
        });
        if (principalResult.isWrong()) {
            return this.forbidden(res, principalResult.value.message);
        }

        req.principal = principalResult.value;

        return next();
    }
}

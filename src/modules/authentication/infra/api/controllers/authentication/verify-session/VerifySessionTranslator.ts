import { Token } from '../../../../../domain/model/Token';

type VerifySessionResponseDTO = {
	userId: string;
	sessionId: string;
	expiration: string;
	type: string;
}

export class VerifySessionTranslator {
	public toVerifySessionResponseDTO(token: Token): VerifySessionResponseDTO {
		return {
			userId: token.userId.value,
			sessionId: token.sessionId.value,
			expiration: token.expiration,
			type: token.type
		};
	}
}

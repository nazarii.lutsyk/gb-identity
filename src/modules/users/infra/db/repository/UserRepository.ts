import { Knex } from 'knex';
import { v4 } from 'uuid';
import { BusinessError, NotFoundError } from '../../../../../shared/core/BusinessError';
import { Either, right, wrong } from '../../../../../shared/core/Result';
import { Email } from '../../../../../shared/domain/Email';
import knexConnection from '../../../../../shared/infra/database/mysql/connection';
import { SessionModel } from '../../../../authentication/infra/db/models/SessionModel';
import { User } from '../../../domain/model/User';
import { UserId } from '../../../domain/model/UserId';
import { UserRepository } from '../../../domain/repository/UserRepository';
import { UserMapper } from '../../mappers/UserMapper';
import { UserModel } from '../models/UserModel';
import QueryBuilder = Knex.QueryBuilder;

const USER_TABLE_NAME = 'User';

class Repository implements UserRepository {
	public nextIdentifier(): UserId {
		return new UserId(v4().replace(/-/ig, ''));
	}

	public async getUserByEmail(email: Email): Promise<Either<BusinessError | NotFoundError, User>> {
		try {
			const user = await this.db.where<UserModel>('email', email.value).first();
			if (!user) {
				return wrong(new NotFoundError(email.value, USER_TABLE_NAME));
			}
			return right(UserMapper.toDomain(user));
		} catch (e) {
			return wrong(new BusinessError((e as Error).message));
		}
	}

	public async saveUser(user: User): Promise<Either<BusinessError, UserId>> {
		try {
			const userModel = UserMapper.toPersistence(user);
			const result = await this.db.insert<SessionModel>(userModel);
			return right(new UserId(result.id));
		} catch (e) {
			return wrong(new BusinessError((e as Error).message));
		}
	}

	public async userExistsByEmail(email: Email): Promise<Either<BusinessError, boolean>> {
		try {
			const user = await this.db.select('id').where<UserModel>('email', email.value).first();
			return right(Boolean(user));
		} catch (e) {
			return wrong(new BusinessError((e as Error).message));
		}
	}


	private get db(): QueryBuilder {
		return knexConnection(USER_TABLE_NAME);
	}
}

export function createUserRepository(): UserRepository {
	return new Repository();
}

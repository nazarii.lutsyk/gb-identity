import { Right } from '../../../../shared/core/Result';
import { Email } from '../../../../shared/domain/Email';
import { User } from '../../domain/model/User';
import { UserId } from '../../domain/model/UserId';
import { UserModel } from '../db/models/UserModel';

export class UserMapper {
    public static toPersistence(user: User): UserModel {
        return {
            id: user.id.value,
            email: user.email.value
        };
    }

    public static toDomain(user: UserModel): User {
        const emailResult = Email.create(user.email) as Right<never, Email>;
        return new User(new UserId(user.id), {email: emailResult.value});
    }
}

import { GoogleAccountRepresentation } from './GoogleAccountRepresentation';
import { Identity } from '../../../domain/model/Identity';
import { Either, right, wrong } from '../../../../../shared/core/Result';
import { ValidationError } from '../../../../../shared/core/BusinessError';
import { Email } from '../../../../../shared/domain/Email';

export class GoogleAccountTranslator {
	toAccountFromRepresentation(
		representation: GoogleAccountRepresentation.Response
	): Either<ValidationError, Identity> {
		const emailResult = Email.create(representation.data.emailAddresses[0].value);
		if (emailResult.isWrong()) {
			return wrong(emailResult.value);
		}

		const accountResult = Identity.create({
			firstName: representation.data.names[0].givenName,
			lastName: representation.data.names[0].familyName,
			photoUrl: representation.data.photos[0].url,
			email: emailResult.value,
		});
		if (accountResult.isWrong()) {
			return wrong(accountResult.value);
		}

		return right(accountResult.value);
	}
}

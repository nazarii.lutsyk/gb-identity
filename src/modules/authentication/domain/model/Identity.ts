import { Either, wrong, right } from '../../../../shared/core/Result';
import { Validator } from '../../../../shared/libs/Validator';
import { PropertyRequiredError } from '../../../../shared/core/BusinessError';
import { ValueObject } from '../../../../shared/domain/ValueObject';
import { Email } from '../../../../shared/domain/Email';

type IdentityProps = {
	firstName: string;
	lastName: string;
	email: Email;
	photoUrl?: string;
};

export class Identity extends ValueObject<IdentityProps> {
	public static create(props: IdentityProps): Either<PropertyRequiredError, Identity> {
		const validatorResult = Validator.againstNullOrUndefinedBulk(
			{ key: 'firstName', value: props.firstName },
			{ key: 'lastName', value: props.lastName },
			{ key: 'email', value: props.email }
		);
		if (validatorResult.isWrong()) {
			return wrong(new PropertyRequiredError(validatorResult.value.properties));
		}

		return right(new Identity(props));
	}

	get email(): Email {
		return this.props.email;
	}
}

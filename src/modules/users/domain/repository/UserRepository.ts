import { BusinessError, NotFoundError } from '../../../../shared/core/BusinessError';
import { Either } from '../../../../shared/core/Result';
import { Email } from '../../../../shared/domain/Email';
import { User } from '../model/User';
import { UserId } from '../model/UserId';

export interface UserRepository {
    nextIdentifier(): UserId;
    saveUser(user: User): Promise<Either<BusinessError, UserId>>;
    userExistsByEmail(email: Email): Promise<Either<BusinessError, boolean>>;
    getUserByEmail(email: Email): Promise<Either<BusinessError|NotFoundError, User>>;
}

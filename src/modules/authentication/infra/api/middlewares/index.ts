import { verifySessionUseCase } from '../../../application';
import { VerifySessionMiddleware } from './verify-session/VerifySessionMiddleware';

export const verifySessionMiddleware = new VerifySessionMiddleware(verifySessionUseCase);

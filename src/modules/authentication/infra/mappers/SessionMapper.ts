import { UserId } from '../../../users/domain/model/UserId';
import { Session } from '../../domain/model/Session';
import { SessionId } from '../../domain/model/SessionId';
import { SessionModel } from '../db/models/SessionModel';

export class SessionMapper {
    public static toDomain(session: SessionModel): Session {
        return new Session(new SessionId(session.id), {userId: new UserId(session.user_id)});
    }

    public static toPersistence(session: Session): SessionModel {
        return {
            id: session.id.value,
            user_id: session.userId.value
        };
    }
}

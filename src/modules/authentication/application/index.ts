import { userRepository } from '../../users/infra/db/repository';
import { authenticationService } from '../domain/model/services';
import { sessionRepository } from '../infra/db/repository';
import googleAuthenticationClient from '../infra/services/google-authentication';
import googleAuthentication from '../infra/services/google-authentication';
import { jwtService } from '../infra/services/jwt';
import { RefreshSessionUseCase } from './refresh-session/RefreshSessionUseCase';
import { ProcessAuthenticationUseCase } from './sign-in/processAuthentication/ProcessAuthenticationUseCase';
import { StartAuthenticationUseCase } from './sign-in/startAuthentication/StartAuthenticationUseCase';
import { SignOutUseCase } from './sign-out/SignOutUseCase';
import { VerifySessionUseCase } from './verify-session/VerifySessionUseCase';

export const startAuthenticationUseCase = new StartAuthenticationUseCase(googleAuthentication);
export const processAuthenticationUseCase = new ProcessAuthenticationUseCase(
    googleAuthenticationClient,
    authenticationService,
    userRepository,
    sessionRepository
);

export const signOutUseCase = new SignOutUseCase(sessionRepository);

export const verifySessionUseCase = new VerifySessionUseCase(sessionRepository, jwtService);

export const refreshSessionUseCase = new RefreshSessionUseCase(jwtService, authenticationService, sessionRepository);

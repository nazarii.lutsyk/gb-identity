import { BusinessError, ValidationError } from '../../../../shared/core/BusinessError';
import { Either } from '../../../../shared/core/Result';
import { UserId } from '../../../users/domain/model/UserId';
import { SessionId } from '../model/SessionId';
import { Token, TokenType } from '../model/Token';

export interface TokenService {
	generateToken(
		userId: UserId,
		sessionId: SessionId,
		type: TokenType,
		expiration: string
	): Either<ValidationError, Token>;
	verifyToken(token: string, ignoreExpiration?: boolean): Either<ValidationError | BusinessError, Token>;
}

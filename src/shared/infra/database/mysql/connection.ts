import knex from 'knex';
import knexConfig from './knexfile';
import Log from '../../../libs/Logger';

Log.info('connecting to the database');
const knexConnection = knex(knexConfig);
Log.info('successfully connected to the database');

export default knexConnection;

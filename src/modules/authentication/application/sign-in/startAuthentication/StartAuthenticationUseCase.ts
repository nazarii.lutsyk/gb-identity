import { UseCase } from '../../../../../shared/domain/UseCase';
import { IdentityService } from '../../../domain/proxy/IdentityService';
import Log from '../../../../../shared/libs/Logger';

export class StartAuthenticationUseCase implements UseCase<null, string> {
	private googleAuthenticationService: IdentityService;

	constructor(googleAuthenticationProxy: IdentityService) {
		this.googleAuthenticationService = googleAuthenticationProxy;
	}

	public async execute(_request?: null): Promise<string> {
		Log.info('starting authentication process');
		return this.googleAuthenticationService.authenticationUrl();
	}
}

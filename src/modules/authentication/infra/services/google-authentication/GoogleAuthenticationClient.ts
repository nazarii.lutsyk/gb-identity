import { OAuth2Client } from 'google-auth-library';
import { google } from 'googleapis';
import { APIEndpoint } from 'googleapis-common';
import { Identity } from '../../../domain/model/Identity';
import { GoogleAccountRepresentation } from './GoogleAccountRepresentation';
import { GoogleAccountTranslator } from './GoogleAccountTranslator';
import { Either, wrongWithMessage, right } from '../../../../../shared/core/Result';
import { ValidationError } from '../../../../../shared/core/BusinessError';

type GoogleAuthenticationClientProps = {
	clientId: string;
	clientSecret: string;
	clientRedirectUrl: string;
};

const DEFAULT_SCOPE = [
	'https://www.googleapis.com/auth/userinfo.profile',
	'https://www.googleapis.com/auth/userinfo.email',
];

export class GoogleAuthenticationClient {
	protected constructor(private client: OAuth2Client) {}

	static create(props: GoogleAuthenticationClientProps): GoogleAuthenticationClient {
		if (props == null) {
			throw new Error('props are required for Google authentication services creation');
		}
		if (!props.clientId || !props.clientSecret || !props.clientRedirectUrl) {
			throw new Error('missing some properties required for Google authentication services creation');
		}

		return new GoogleAuthenticationClient(
			new google.auth.OAuth2(props.clientId, props.clientSecret, props.clientRedirectUrl)
		);
	}

	public async accountFromCode(code: string): Promise<Either<ValidationError, Identity>> {
		const { tokens } = await this.client.getToken(code);
		this.client.setCredentials(tokens);

		const me: GoogleAccountRepresentation.Response = await this.googlePlusClient().people.get({
			resourceName: 'people/me',
			personFields: 'birthdays,clientData,emailAddresses,genders,names,photos',
		});

		const accountResult = new GoogleAccountTranslator().toAccountFromRepresentation(me);
		if (accountResult.isWrong()) {
			return wrongWithMessage(accountResult.value, 'unable to translate google account response');
		}

		return right(accountResult.value);
	}

	public authenticationUrl(): string {
		return this.client.generateAuthUrl({
			access_type: 'offline',
			prompt: 'consent',
			scope: DEFAULT_SCOPE,
		});
	}

	private googlePlusClient(): APIEndpoint {
		return google.people({ version: 'v1', auth: this.client });
	}
}

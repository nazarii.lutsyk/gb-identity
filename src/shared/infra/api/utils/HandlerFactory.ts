import { Handler, NextFunction, Request, Response } from 'express';
import { BaseController } from '../controllers/BaseController';

const CreateHandler = (controller: BaseController): Handler => {
	return (req: Request, res: Response, next?: NextFunction) => controller.execute(req, res, next);
};

export default CreateHandler;

import config from '../config/config';
import Log from './libs/Logger';
import './infra/database/mysql/connection';
import createServer from './infra/api/server';

Log.info('starting application');

const httpPort = config.get('server.port') || 3000;
createServer().listen(httpPort, () => {
	Log.info(`HTTP server started listening port ${httpPort}`);
});

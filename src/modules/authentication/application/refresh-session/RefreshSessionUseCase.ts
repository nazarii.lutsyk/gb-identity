import { BusinessError } from '../../../../shared/core/BusinessError';
import { Either, right, wrong } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import Log from '../../../../shared/libs/Logger';
import { AuthenticationService } from '../../domain/model/services/AuthenticationService';
import { TokenPair, TokenType } from '../../domain/model/Token';
import { SessionRepository } from '../../domain/repository/SessionRepository';
import { TokenService } from '../../domain/services/TokenService';

export type RefreshSessionCommand = {
    accessToken: string;
    refreshToken: string;
}
type Response = Either<BusinessError, TokenPair>

export class RefreshSessionUseCase implements UseCase<RefreshSessionCommand, Response> {
    private tokenService: TokenService;
    private authenticationService: AuthenticationService;
    private sessionRepository: SessionRepository;

    constructor(tokenService: TokenService, authenticationService: AuthenticationService, sessionRepository: SessionRepository) {
        this.tokenService = tokenService;
        this.authenticationService = authenticationService;
        this.sessionRepository = sessionRepository;
    }

    public async execute(command: RefreshSessionCommand): Promise<Response> {
        Log.info('starting refresh session process');

        const accessTokenVerificationResult = this.tokenService.verifyToken(command.accessToken, true);
        if (accessTokenVerificationResult.isWrong()) {
            return wrong(accessTokenVerificationResult.value);
        }

        const refreshTokenVerificationResult = this.tokenService.verifyToken(command.refreshToken, false);
        if (refreshTokenVerificationResult.isWrong()) {
            return wrong(refreshTokenVerificationResult.value);
        }
        if (refreshTokenVerificationResult.value.type !== TokenType.REFRESH){
            return wrong(new BusinessError('invalid refresh token type'));
        }

        const sessionResult = await this.sessionRepository.sessionById(accessTokenVerificationResult.value.sessionId);
        if (sessionResult.isWrong()) {
            return wrong(sessionResult.value);
        }

        const refreshResult = this.authenticationService.refresh(sessionResult.value);
        if (refreshResult.isWrong()) {
            return wrong(refreshResult.value);
        }

        return right(refreshResult.value.tokenPair() as TokenPair);
    }
}

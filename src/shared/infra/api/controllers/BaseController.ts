import { NextFunction, Request, Response } from 'express';
import Log from '../../../libs/Logger';

export abstract class BaseController {

	protected abstract handle(req: Request, res: Response, next?: NextFunction): Promise<unknown>;

	public async execute(req: Request, res: Response, next?: NextFunction): Promise<void> {
		try {
			await this.handle(req, res, next);
		} catch (err) {
			Log.error('[BaseController]: Uncaught controller error');
			Log.error(err);
			this.fail(res, 'An unexpected error occurred');
		}
	}

	public static send(res: Response, code: number, message: string) {
		return res.status(code).json({ message });
	}

	public ok<T>(res: Response, dto?: T) {
		if (dto) {
			res.type('application/json');
			return res.status(200).json(dto);
		} else {
			return res.sendStatus(200);
		}
	}

	public created(res: Response) {
		return res.sendStatus(201);
	}

	public badRequest(res: Response, message?: string) {
		return BaseController.send(res, 400, message || 'Bad request');
	}

	public unauthorized(res: Response, message?: string) {
		return BaseController.send(res, 401, message || 'Unauthorized');
	}

	public forbidden(res: Response, message?: string) {
		return BaseController.send(res, 403, message || 'Forbidden');
	}

	public notFound(res: Response, message?: string) {
		return BaseController.send(res, 404, message || 'Not found');
	}

	public fail(res: Response, error: Error | string) {
		return res.status(500).json({
			message: error.toString(),
		});
	}
}

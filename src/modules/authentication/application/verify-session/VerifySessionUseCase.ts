import { BusinessError } from '../../../../shared/core/BusinessError';
import { Either, right, wrong } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import Log from '../../../../shared/libs/Logger';
import { Token } from '../../domain/model/Token';
import { SessionRepository } from '../../domain/repository/SessionRepository';
import { TokenService } from '../../domain/services/TokenService';

type Response = Either<BusinessError, Token>

export class VerifySessionUseCase implements UseCase<string, Response> {

    private sessionRepository: SessionRepository;
    private tokenService: TokenService;

    constructor(sessionRepository: SessionRepository, tokenService: TokenService) {
        this.sessionRepository = sessionRepository;
        this.tokenService = tokenService;
    }

    public async execute(token: string): Promise<Response> {
        Log.info('starting session verification process');

        const tokenResult = this.tokenService.verifyToken(token);
        if (tokenResult.isWrong()){
            return wrong(tokenResult.value);
        }

        const sessionExistsResult = await this.sessionRepository.sessionExistsById(tokenResult.value.sessionId);
        if (sessionExistsResult.isWrong()){
            return wrong(sessionExistsResult.value);
        }
        if (!sessionExistsResult.value) {
            return wrong(new BusinessError('session does not exists'));
        }

        return right(tokenResult.value);
    }
}

import { Either, wrong, right } from '../core/Result';

export type ValidatorFailure = {
	errorMessage: string;
}

export type NullOfUndefinedFailure = ValidatorFailure & {
	property: string;
}

export type NullOfUndefinedBulkFailure = ValidatorFailure & {
	properties: string[];
}

export type IValidatorArgument = {
	key: string;
	value: unknown;
}

export class Validator {
	public static againstNullOrUndefined(value: unknown, key: string): Either<NullOfUndefinedFailure, null> {
		if (value === null || value === undefined || value === '') {
			return wrong({
				property: key,
				errorMessage: `${key} is missing or empty`,
			});
		}
		return right(null);
	}

	public static againstNullOrUndefinedBulk(...args: IValidatorArgument[]): Either<NullOfUndefinedBulkFailure, null> {
		const failureResult: NullOfUndefinedBulkFailure = {
			properties: [],
			errorMessage: '',
		};

		for (const arg of args) {
			const result = this.againstNullOrUndefined(arg.value, arg.key);
			if (result.isRight()) continue;

			failureResult.errorMessage = `${failureResult.errorMessage}, ${result.value.errorMessage}`;
			failureResult.properties.push(result.value.property);
		}

		if (failureResult.properties.length > 0) {
			return wrong(failureResult);
		}

		return right(null);
	}
}

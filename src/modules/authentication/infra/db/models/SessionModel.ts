export class SessionModel {
  public id: string;
  public user_id: string;


  constructor(id: string, userId: string) {
    this.id = id;
    this.user_id = userId;
  }
}

import { BusinessError } from '../../../../../shared/core/BusinessError';
import { Either, right, wrongWithMessage } from '../../../../../shared/core/Result';
import { UseCase } from '../../../../../shared/domain/UseCase';
import Log from '../../../../../shared/libs/Logger';
import { User } from '../../../../users/domain/model/User';
import { UserId } from '../../../../users/domain/model/UserId';
import { UserRepository } from '../../../../users/domain/repository/UserRepository';
import { AuthenticationService } from '../../../domain/model/services/AuthenticationService';
import { TokenPair } from '../../../domain/model/Token';
import { IdentityService } from '../../../domain/proxy/IdentityService';
import { SessionRepository } from '../../../domain/repository/SessionRepository';

type Response = Either<BusinessError, TokenPair>;

export class ProcessAuthenticationUseCase implements UseCase<string, Response> {
  constructor(
    private identityService: IdentityService,
    private authenticationService: AuthenticationService,
    private userRepository: UserRepository,
    private sessionRepository: SessionRepository
  ) {
    this.identityService = identityService;
  }

  public async execute(authenticationCode: string): Promise<Response> {
    Log.info(`Processing authentication for ${authenticationCode}`);

    const identityResult = await this.identityService.identityFromCode(authenticationCode);
    if (identityResult.isWrong()) {
      return wrongWithMessage(identityResult.value, 'unable to get google identity');
    }

    const { email } = identityResult.value;

    let userId: UserId = this.userRepository.nextIdentifier();

    const userResult = await this.userRepository.getUserByEmail(email);
    if (userResult.isWrong()) {
      const userResult = User.create(userId, { email });
      if (userResult.isWrong()) {
        return wrongWithMessage(userResult.value, 'unable to create user aggregate');
      }

      await this.userRepository.saveUser(userResult.value);
    } else {
      userId = userResult.value.id;
    }

    Log.info(`authenticating user ${email.value}`);
    const authenticationResult = await this.authenticationService.authenticate(userId);
    if (authenticationResult.isWrong()) {
      return wrongWithMessage(authenticationResult.value, `unable to authenticate ${userId}`);
    }

    const sessionResult = await this.sessionRepository.saveSession(authenticationResult.value);
    if (sessionResult.isWrong()) {
      return wrongWithMessage(sessionResult.value, `unable to save session ${authenticationResult.value.id}`);
    }

    return right(authenticationResult.value.tokenPair() || new Map());
  }
}

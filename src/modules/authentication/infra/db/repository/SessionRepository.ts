import { Knex } from 'knex';
import { v4 } from 'uuid';
import { BusinessError, NotFoundError } from '../../../../../shared/core/BusinessError';
import { Either, right, wrong } from '../../../../../shared/core/Result';
import knexConnection from '../../../../../shared/infra/database/mysql/connection';
import { Session } from '../../../domain/model/Session';
import { SessionId } from '../../../domain/model/SessionId';
import { SessionRepository } from '../../../domain/repository/SessionRepository';
import { SessionMapper } from '../../mappers/SessionMapper';
import { SessionModel } from '../models/SessionModel';
import QueryBuilder = Knex.QueryBuilder;

const SESSION_TABLE_NAME = 'Session';

class Repository implements SessionRepository {
    public nextIdentifier(): SessionId {
        return new SessionId(v4().replace(/-/ig, ''));
    }

    public async saveSession(session: Session): Promise<Either<BusinessError, SessionId>> {
        try {
            const sessionModel = SessionMapper.toPersistence(session);
            const result = await this.db.insert<SessionModel>(sessionModel);
            return right(new SessionId(result.id));
        } catch (e) {
            return wrong(new BusinessError((e as Error).message));
        }
    }

    public async deleteSessionById(sessionId: SessionId): Promise<Either<BusinessError, null>> {
        try {
            await this.db.where('id', sessionId.value).delete();
            return right(null);
        } catch (e) {
            return wrong(new BusinessError((e as Error).message));
        }
    }

    public async sessionExistsById(sessionId: SessionId): Promise<Either<BusinessError, boolean>> {
        try {
            const session = await this.db.select('id').where<SessionModel>('id', sessionId.value).first();
            return right(Boolean(session));
        } catch (e) {
            return wrong(new BusinessError((e as Error).message));
        }
    }

    public async sessionById(sessionId: SessionId): Promise<Either<BusinessError, Session>> {
        try {
            const session = await this.db.where<SessionModel>('id', sessionId.value).first();
            if (!session) {
                return wrong(new NotFoundError(sessionId.value, SESSION_TABLE_NAME));
            }
            return right(SessionMapper.toDomain(session));
        } catch (e) {
            return wrong(new BusinessError((e as Error).message));
        }
    }

    private get db(): QueryBuilder {
        return knexConnection(SESSION_TABLE_NAME);
    }
}


export function createSessionRepository(): SessionRepository {
    return new Repository();
}

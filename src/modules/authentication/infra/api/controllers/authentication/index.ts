import {
    processAuthenticationUseCase,
    refreshSessionUseCase,
    signOutUseCase,
    startAuthenticationUseCase,
    verifySessionUseCase
} from '../../../../application';
import { jwtService } from '../../../services/jwt';
import { RefreshSessionController } from './refresh-session/RefreshSessionController';
import { ProcessAuthenticationController } from './sign-in/process-authentication/ProcessAuthenticationController';
import { StartAuthenticationController } from './sign-in/start-authentication/StartAuthenticationController';
import { SignOutController } from './sign-out/SignOutController';
import { VerifySessionController } from './verify-session/VerifySessionController';

export const startAuthenticationController = new StartAuthenticationController(startAuthenticationUseCase);
export const processAuthenticationController = new ProcessAuthenticationController(processAuthenticationUseCase);
export const signOutController = new SignOutController(signOutUseCase, jwtService);
export const verifySessionController = new VerifySessionController(verifySessionUseCase);
export const refreshSessionController = new RefreshSessionController(refreshSessionUseCase);


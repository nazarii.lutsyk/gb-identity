import { ValueObject } from './ValueObject';
import { Either, right, wrong } from '../core/Result';
import { PropertyInvalidError } from '../core/BusinessError';

type EmailProps = {
	email: string;
};

export class Email extends ValueObject<EmailProps> {
	get value(): string {
		return this.props.email;
	}

	private static isValidEmail(email: string) {
		const re =
			/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	private static format(email: string): string {
		return email.trim().toLowerCase();
	}

	public static create(email: string): Either<PropertyInvalidError, Email> {
		if (!Email.isValidEmail(email)) {
			return wrong(new PropertyInvalidError(['email'], 'does not comply with regex'));
		} else {
			return right(new Email({ email: Email.format(email) }));
		}
	}
}

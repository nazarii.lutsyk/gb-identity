import { BusinessError } from './BusinessError';

export type Either<W, R> = Wrong<W, R> | Right<W, R>;

export class Wrong<W, R> {
	readonly value: W;

	constructor(value: W) {
		this.value = value;
	}

	isWrong(): this is Wrong<W, R> {
		return true;
	}

	isRight(): this is Right<W, R> {
		return false;
	}
}

export class Right<W, R> {
	readonly value: R;

	constructor(value: R) {
		this.value = value;
	}

	isWrong(): this is Wrong<W, R> {
		return false;
	}

	isRight(): this is Right<W, R> {
		return true;
	}
}

export const wrong = <W, R>(reason: W): Either<W, R> => {
	return new Wrong(reason);
};

export const wrongWithMessage = <W, R>(reason: W, message: string): Either<W, R> => {
	if (reason instanceof BusinessError) {
		reason.message = `${message} because ${reason.message}`;
	}
	return new Wrong(reason);
};

export const right = <W, R>(result: R): Either<W, R> => {
	return new Right<W, R>(result);
};

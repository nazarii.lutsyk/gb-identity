import config from '../../../../../config/config';
import { ValidationError } from '../../../../../shared/core/BusinessError';
import { Either, right, wrong, wrongWithMessage } from '../../../../../shared/core/Result';
import { UserId } from '../../../../users/domain/model/UserId';
import { SessionRepository } from '../../repository/SessionRepository';
import { TokenService } from '../../services/TokenService';
import { Session } from '../Session';
import { SessionId } from '../SessionId';
import { Token, TokenType } from '../Token';

export class AuthenticationService {
	constructor(private tokenService: TokenService, private sessionRepository: SessionRepository) {}

	public authenticate(userId: UserId): Either<ValidationError, Session> {
		const sessionId = this.sessionRepository.nextIdentifier();
		const sessionResult = Session.create(sessionId, { userId });
		if (sessionResult.isWrong()) {
			return wrongWithMessage(sessionResult.value, 'unable to create session');
		}

		const accessTokenResult = this.generateAccessToken(userId, sessionId);
		if (accessTokenResult.isWrong()) {
			return wrong(accessTokenResult.value);
		}

		const refreshTokenResult = this.generateRefreshToken(userId, sessionId);
		if (refreshTokenResult.isWrong()) {
			return wrong(refreshTokenResult.value);
		}

		sessionResult.value.assignTokenPair(accessTokenResult.value, refreshTokenResult.value);

		return right(sessionResult.value);
	}

	public refresh(session: Session): Either<ValidationError, Session> {
		const accessTokenResult = this.generateAccessToken(session.userId, session.id);
		if (accessTokenResult.isWrong()) {
			return wrong(accessTokenResult.value);
		}

		const refreshTokenResult = this.generateRefreshToken(session.userId, session.id);
		if (refreshTokenResult.isWrong()) {
			return wrong(refreshTokenResult.value);
		}

		const refreshedSessionResult = Session.create(session.id, {userId: session.userId });
		if (refreshedSessionResult.isWrong()) {
			return wrong(refreshedSessionResult.value);
		}

		refreshedSessionResult.value.assignTokenPair(accessTokenResult.value, refreshTokenResult.value);

		return right(refreshedSessionResult.value);
	}

	private generateAccessToken(userId: UserId, sessionId: SessionId): Either<ValidationError, Token> {
		const accessTokenResult = this.tokenService.generateToken(
			userId,
			sessionId,
			TokenType.ACCESS,
			config.get('tokens.access.expiration')
		);
		if (accessTokenResult.isWrong()) {
			return wrongWithMessage(accessTokenResult.value, 'unable to generate access token');
		}

		return right(accessTokenResult.value);
	}

	private generateRefreshToken(userId: UserId, sessionId: SessionId): Either<ValidationError, Token> {
		const refreshTokenResult = this.tokenService.generateToken(
			userId,
			sessionId,
			TokenType.REFRESH,
			config.get('tokens.refresh.expiration')
		);
		if (refreshTokenResult.isWrong()) {
			return wrongWithMessage(refreshTokenResult.value, 'unable to generate refresh token');
		}

		return right(refreshTokenResult.value);
	}
}

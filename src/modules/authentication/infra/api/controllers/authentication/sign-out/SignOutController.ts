import { Request, Response } from 'express';
import { BaseController } from '../../../../../../../shared/infra/api/controllers/BaseController';
import { SignOutUseCase } from '../../../../../application/sign-out/SignOutUseCase';
import { SessionId } from '../../../../../domain/model/SessionId';
import { TokenService } from '../../../../../domain/services/TokenService';

export class SignOutController extends BaseController {
    signOutUseCase: SignOutUseCase;
	tokenService: TokenService;

    constructor(signOutUseCase: SignOutUseCase, tokenService: TokenService) {
        super();
        this.signOutUseCase = signOutUseCase;
		this.tokenService = tokenService;
    }

    protected async handle(req: Request, res: Response): Promise<unknown> {
        const result = await this.signOutUseCase.execute(new SessionId(req.principal.sessionId));
		if (result.isWrong()) {
			return this.fail(res, result.value);
		}

        return this.ok(res, {success: true});
    }
}

import { Request, Response } from 'express';

import {
	ProcessAuthenticationUseCase,
} from '../../../../../../application/sign-in/processAuthentication/ProcessAuthenticationUseCase';
import { BaseController } from '../../../../../../../../shared/infra/api/controllers/BaseController';
import { ProcessAuthenticationTranslator } from './ProcessAuthenticationTranslator';

export class ProcessAuthenticationController extends BaseController {
	processAuthenticationUseCase: ProcessAuthenticationUseCase;

	constructor(processAuthenticationUseCase: ProcessAuthenticationUseCase) {
		super();
		this.processAuthenticationUseCase = processAuthenticationUseCase;
	}

	protected async handle(req: Request, res: Response): Promise<unknown> {
		const authenticationCode = String(req.query['code']);
		if (!authenticationCode) {
			return this.badRequest(res, 'code is required');
		}

		const authenticationInfo = await this.processAuthenticationUseCase.execute(authenticationCode);
		if (authenticationInfo.isWrong()) {
			return this.fail(res, authenticationInfo.value);
		}

		return this.ok(res, new ProcessAuthenticationTranslator().toProcessAuthenticationResponseDTO(authenticationInfo.value));
	}
}

import { BusinessError } from '../../../../shared/core/BusinessError';
import { Either, right, wrong } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/domain/UseCase';
import Log from '../../../../shared/libs/Logger';
import { SessionId } from '../../domain/model/SessionId';
import { SessionRepository } from '../../domain/repository/SessionRepository';

type Response = Either<BusinessError, boolean>

export class SignOutUseCase implements UseCase<SessionId, Response> {

    private sessionRepository: SessionRepository;

    constructor(sessionRepository: SessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public async execute(sessionId: SessionId): Promise<Response> {
        Log.info('starting sign out process');

        const result = await this.sessionRepository.deleteSessionById(sessionId);
        if (result.isWrong()) {
            return wrong(result.value);
        }

        return right(true);
    }
}

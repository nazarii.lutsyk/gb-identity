import { Request, Response } from 'express';
import { BaseController } from '../../../../../../../shared/infra/api/controllers/BaseController';
import { RefreshSessionUseCase } from '../../../../../application/refresh-session/RefreshSessionUseCase';
import { RefreshSessionTranslator } from './RefreshSessionTranslator';

export class RefreshSessionController extends BaseController {
    refreshSessionUseCase: RefreshSessionUseCase;

    constructor(refreshSessionUseCase: RefreshSessionUseCase) {
        super();
        this.refreshSessionUseCase = refreshSessionUseCase;
    }

    protected async handle(req: Request, res: Response): Promise<unknown> {
        const accessToken = req.headers['authorization'] as string;
        if (!accessToken) {
            return this.badRequest(res, 'access token is required');
        }

        const refreshToken = req.headers['refresh'] as string;
        if (!refreshToken) {
            return this.badRequest(res, 'refresh token is required');
        }

        const result = await this.refreshSessionUseCase.execute({accessToken, refreshToken});
		if (result.isWrong()) {
			return this.fail(res, result.value);
		}

        return this.ok(res, new RefreshSessionTranslator().toRefreshSessionResponseDTO(result.value));
    }
}

import { Request, Response } from 'express';
import { BaseController } from '../../../../../../../../shared/infra/api/controllers/BaseController';
import { StartAuthenticationUseCase } from '../../../../../../application/sign-in/startAuthentication/StartAuthenticationUseCase';

export class StartAuthenticationController extends BaseController {
	startAuthenticationUseCase: StartAuthenticationUseCase;

	constructor(startAuthenticationUseCase: StartAuthenticationUseCase) {
		super();
		this.startAuthenticationUseCase = startAuthenticationUseCase;
	}

	protected async handle(_req: Request, res: Response): Promise<void> {
		const authenticationUrl = await this.startAuthenticationUseCase.execute();
		return res.redirect(authenticationUrl);
	}
}

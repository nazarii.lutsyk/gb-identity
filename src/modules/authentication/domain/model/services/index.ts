import { AuthenticationService } from './AuthenticationService';
import { jwtService } from '../../../infra/services/jwt';
import { sessionRepository } from '../../../infra/db/repository';

export const authenticationService = new AuthenticationService(jwtService, sessionRepository);

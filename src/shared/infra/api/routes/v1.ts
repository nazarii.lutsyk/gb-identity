import { Router } from 'express';
import AuthenticationRouter from '../../../../modules/authentication/infra/api/routes/AuthenticationRouter';

const V1Router = Router();

V1Router.use('/authentication', AuthenticationRouter);

export default V1Router;

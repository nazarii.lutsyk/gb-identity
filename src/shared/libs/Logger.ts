import moment from 'moment';
import { createLogger, format, transports } from 'winston';
import config from '../../config/config';

const { combine, timestamp, prettyPrint, splat, printf, colorize } = format;

const defaultFormat = printf(({ message, stack, level, timestamp: t }) => {
	let text = typeof message === 'object' ? JSON.stringify(message) : message;

	if (stack) {
		text = stack;
	}

	const formattedDate = moment(t).format('DD-MM-YYYY HH:mm:ss |').toString();

	return `${formattedDate} ${config.get('env')} ${level}: ${text}`;
});

const Log = createLogger({
	level: 'silly',
	format: combine(
		timestamp(),
		prettyPrint(),
		splat(),
		colorize({ all: true }),
		defaultFormat
	),
	transports: [new transports.Console()],
	exitOnError: false
});

export default Log;

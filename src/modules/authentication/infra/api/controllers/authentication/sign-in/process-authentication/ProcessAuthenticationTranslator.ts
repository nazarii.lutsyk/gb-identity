import { TokenPair, TokenType } from '../../../../../../domain/model/Token';

type ProcessAuthenticationResponseDTO = {
	access: string;
	refresh: string;
}

export class ProcessAuthenticationTranslator {
	public toProcessAuthenticationResponseDTO(tokens: TokenPair): ProcessAuthenticationResponseDTO {
		return {
			access: tokens.get(TokenType.ACCESS)?.stringifiedToken || '',
			refresh:  tokens.get(TokenType.REFRESH)?.stringifiedToken || '',
		};
	}
}

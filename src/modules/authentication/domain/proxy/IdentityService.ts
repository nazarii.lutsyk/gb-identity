import { ValidationError } from '../../../../shared/core/BusinessError';
import { Either } from '../../../../shared/core/Result';
import { Identity } from '../model/Identity';

export interface IdentityService {
	authenticationUrl(): string;
	identityFromCode(code: string): Promise<Either<ValidationError, Identity>>;
}

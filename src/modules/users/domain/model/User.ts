import { PropertyRequiredError } from '../../../../shared/core/BusinessError';
import { Either, right, wrong } from '../../../../shared/core/Result';
import { AggregateRoot } from '../../../../shared/domain/AggregateRoot';
import { Email } from '../../../../shared/domain/Email';
import { Validator } from '../../../../shared/libs/Validator';
import { UserId } from './UserId';

type IUserProps = {
    email: Email;
};

export class User extends AggregateRoot<IUserProps> {
    public static create(id: UserId, props: IUserProps): Either<PropertyRequiredError, User> {
        const validatorResult = Validator.againstNullOrUndefined(props.email, 'email');
        if (validatorResult.isWrong()) {
            wrong(new PropertyRequiredError([validatorResult.value.property]));
        }

        return right(new User(id, props));
    }

    get id(): UserId {
        return this._id;
    }

    get email(): Email {
		return this.props.email;
    }
}

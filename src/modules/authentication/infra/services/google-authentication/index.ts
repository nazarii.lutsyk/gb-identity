import { GoogleAuthenticationService } from './GoogleAuthenticationService';
import config from '../../../../../config/config';
import { GoogleAuthenticationClient } from './GoogleAuthenticationClient';

const googleAuthenticationClient = GoogleAuthenticationClient.create({
		clientId: config.get('google.clientId'),
		clientSecret: config.get('google.clientSecret'),
		clientRedirectUrl: config.get('google.clientRedirectUrl'),
	},
);

const googleAuthenticationService = new GoogleAuthenticationService(googleAuthenticationClient);

export default googleAuthenticationService;

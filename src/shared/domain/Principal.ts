import { PropertyRequiredError } from '../core/BusinessError';
import { Either, right, wrong } from '../core/Result';
import { Validator } from '../libs/Validator';
import { ValueObject } from './ValueObject';

type PrincipalProps = {
    userId: string;
    sessionId: string;
    type: string;
};

export class Principal extends ValueObject<PrincipalProps>{
    static create(props: PrincipalProps): Either<PropertyRequiredError, Principal> {
        const validatorResult = Validator.againstNullOrUndefinedBulk(
            { key: 'userId', value: props.userId },
            { key: 'sessionId', value: props.sessionId },
            { key: 'type', value: props.type },
        );
        if (validatorResult.isWrong()) {
            return wrong(new PropertyRequiredError(validatorResult.value.properties));
        }
        return right(new Principal(props));
    }

    get sessionId(): string {
        return this.props.sessionId;
    }

    get userId(): string {
        return this.props.userId;
    }

    get type(): string {
        return this.props.type;
    }
}

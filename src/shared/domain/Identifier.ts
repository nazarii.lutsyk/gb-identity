export class Identifier {
	private readonly _value: string;

	constructor(value: string | number) {
		this._value = String(value);
	}

	equals(id: Identifier): boolean {
		if (id === null || id === undefined) {
			return false;
		}
		if (!(id instanceof this.constructor)) {
			return false;
		}
		return id.value === this._value;
	}

	get value(): string {
		return this._value;
	}
}

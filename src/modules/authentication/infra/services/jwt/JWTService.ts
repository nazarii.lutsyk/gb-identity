import { decode, JwtPayload, sign, SignOptions, verify } from 'jsonwebtoken';
import { BusinessError, ValidationError } from '../../../../../shared/core/BusinessError';
import { Either, right, wrong, wrongWithMessage } from '../../../../../shared/core/Result';
import { UserId } from '../../../../users/domain/model/UserId';
import { SessionId } from '../../../domain/model/SessionId';
import { Token, TokenType } from '../../../domain/model/Token';
import { TokenString } from '../../../domain/model/TokenString';
import { TokenService } from '../../../domain/services/TokenService';

export class JWTService implements TokenService {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    private publicKey: string;
    private privateKey: string;

    constructor(publicKey: string, privateKey: string) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public generateToken(
        userId: UserId,
        sessionId: SessionId,
        type: TokenType,
        expiration: string
    ): Either<ValidationError, Token> {
        const jwt = this.generateJWTToken({
            userId: userId.value,
            sessionId: sessionId.value,
            type
        }, {expiresIn: expiration});

        const tokenValue = TokenString.create(jwt);
        if (tokenValue.isWrong()) {
            return wrongWithMessage(tokenValue.value, 'unable to generate jwt token value');
        }

        const tokenResult = Token.create({
            userId: userId,
            sessionId: sessionId,
            type: type,
            expiration: expiration,
            value: tokenValue.value,
        });
        if (tokenResult.isWrong()) {
            return wrongWithMessage(tokenResult.value, 'unable to create token value object');
        }

        return right(tokenResult.value);
    }

    public verifyToken(token: string, ignoreExpiration: boolean): Either<ValidationError | BusinessError, Token> {
        try {
            const tokenResult = TokenString.create(token);
            if (tokenResult.isWrong()) {
                return wrong(tokenResult.value);
            }

            verify(tokenResult.value.token, this.privateKey, {ignoreExpiration});

            return this.decodeToken(tokenResult.value);
        } catch (e) {
            return wrong(new BusinessError((e as Error).message));
        }
    }

    private decodeToken(token: TokenString): Either<ValidationError, Token> {
        const payload = decode(token.token) as JwtPayload;
        return Token.create({
            userId: new UserId(payload.userId),
            sessionId: new SessionId(payload.sessionId),
            type: payload.type,
            expiration: String(payload.exp),
            value: token
        });
    }


    private generateJWTToken(data: object, options: SignOptions): string {
        return sign(data, this.privateKey, options);
    }
}

import { Router } from 'express';
import HandlerFactory from '../../../../../shared/infra/api/utils/HandlerFactory';
import {
    processAuthenticationController,
    refreshSessionController,
    signOutController,
    startAuthenticationController,
    verifySessionController
} from '../controllers/authentication';
import { verifySessionMiddleware } from '../middlewares';

const authenticationRouter = Router();

authenticationRouter.get('/google/sign-in', HandlerFactory(startAuthenticationController));
authenticationRouter.get('/google/callback', HandlerFactory(processAuthenticationController));
authenticationRouter.get('/sign-out', HandlerFactory(verifySessionMiddleware), HandlerFactory(signOutController));
authenticationRouter.get('/verify', HandlerFactory(verifySessionController));
authenticationRouter.get('/refresh', HandlerFactory(refreshSessionController));

export default authenticationRouter;

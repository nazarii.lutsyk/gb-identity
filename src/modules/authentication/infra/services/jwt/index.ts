import { JWTService } from './JWTService';
import config from '../../../../../config/config';

export const jwtService = new JWTService(config.get('tokens.publicKey'), config.get('tokens.privateKey'));

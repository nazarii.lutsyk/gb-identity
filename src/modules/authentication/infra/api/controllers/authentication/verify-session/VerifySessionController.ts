import { Request, Response } from 'express';
import { BaseController } from '../../../../../../../shared/infra/api/controllers/BaseController';
import { VerifySessionUseCase } from '../../../../../application/verify-session/VerifySessionUseCase';
import { VerifySessionTranslator } from './VerifySessionTranslator';

export class VerifySessionController extends BaseController {
    verifySessionUseCase: VerifySessionUseCase;

    constructor(verifySessionUseCase: VerifySessionUseCase) {
        super();
        this.verifySessionUseCase = verifySessionUseCase;
    }

    protected async handle(req: Request, res: Response): Promise<unknown> {
        const token = req.headers['authorization'] as string;
        if (!token) {
            return this.badRequest(res, 'token is required');
        }

        const result = await this.verifySessionUseCase.execute(token);
		if (result.isWrong()) {
			return this.fail(res, result.value);
		}

        return this.ok(res, new VerifySessionTranslator().toVerifySessionResponseDTO(result.value));
    }
}

import cors from 'cors';
import express, { Express } from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import morgan from 'morgan';
import Log from '../../libs/Logger';
import V1Router from './routes/v1';

function createServer(): Express {
	const server = express();

	server.use(bodyParser.json());
	server.use(bodyParser.urlencoded({ extended: true }));
	server.use(cors());
	server.use(helmet());
	server.use(
		morgan('dev', {
			stream: {
				write: s => Log.http(s),
			},
		})
	);

	server.use('/v1', V1Router);

	return server;
}

export default createServer;

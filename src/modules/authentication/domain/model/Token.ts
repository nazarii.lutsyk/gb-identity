import { PropertyRequiredError } from '../../../../shared/core/BusinessError';
import { Either, right, wrong } from '../../../../shared/core/Result';
import { ValueObject } from '../../../../shared/domain/ValueObject';
import { Validator } from '../../../../shared/libs/Validator';
import { UserId } from '../../../users/domain/model/UserId';
import { SessionId } from './SessionId';
import { TokenString } from './TokenString';

export enum TokenType {
	ACCESS = 'ACCESS',
	REFRESH = 'REFRESH',
}

export type TokenPair = Map<TokenType, Token>;

type TokenProps = {
	userId: UserId;
	sessionId: SessionId;
	expiration: string;
	type: TokenType;
	value: TokenString;
};

export class Token extends ValueObject<TokenProps> {
	static create(props: TokenProps): Either<PropertyRequiredError, Token> {
		const validatorResult = Validator.againstNullOrUndefinedBulk(
			{ key: 'userId', value: props.userId },
			{ key: 'sessionId', value: props.sessionId },
			{ key: 'expiration', value: props.expiration },
			{ key: 'type', value: props.type },
			{ key: 'value', value: props.value.token }
		);
		if (validatorResult.isWrong()) {
			return wrong(new PropertyRequiredError(validatorResult.value.properties));
		}
		return right(new Token(props));
	}

	get stringifiedToken(): string {
		return this.props.value.token;
	}

	get sessionId(): SessionId {
		return this.props.sessionId;
	}

	get userId(): UserId {
		return this.props.userId;
	}

	get expiration(): string {
		return this.props.expiration;
	}

	get type(): string {
		return this.props.type;
	}
}

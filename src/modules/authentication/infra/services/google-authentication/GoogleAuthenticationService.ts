import { IdentityService } from '../../../domain/proxy/IdentityService';
import Log from '../../../../../shared/libs/Logger';
import { GoogleAuthenticationClient } from './GoogleAuthenticationClient';
import { Identity } from '../../../domain/model/Identity';
import { Either, wrongWithMessage, right } from '../../../../../shared/core/Result';
import { ValidationError } from '../../../../../shared/core/BusinessError';

export class GoogleAuthenticationService implements IdentityService {
	constructor(private client: GoogleAuthenticationClient) {}

	public async identityFromCode(code: string): Promise<Either<ValidationError, Identity>> {
		Log.info(`requesting google account information for ${code}`);

		const identityResult = await this.client.accountFromCode(code);
		if (identityResult.isWrong()) {
			return wrongWithMessage(identityResult.value, `unable to get account by code ${code}`);
		}

		return right(identityResult.value);
	}

	public authenticationUrl(): string {
		Log.info('requesting authentication url');

		return this.client.authenticationUrl();
	}
}

export class BusinessError extends Error {
	constructor(message: string) {
		super(message);
		this.name = this.constructor.name;
	}
}

export class ValidationError extends BusinessError {}

export class PropertyRequiredError extends ValidationError {
	constructor(properties: string[], message?: string) {
		super(`${message} no properties: ${properties.join(', ')}`);
	}
}

export class PropertyInvalidError extends ValidationError {
	constructor(properties: string[], message?: string) {
		super(`${message} invalid: ${properties.join(', ')}`);
	}
}

export class NotFoundError extends BusinessError {
	constructor(id?: string, entity?: string, message?: string) {
		if (message) {
			super(message);
		} else if (id && entity) {
			super(`${entity} with id ${id} not found`);
		} else {
			super('not found');
		}
	}
}

import { TokenPair, TokenType } from '../../../../../domain/model/Token';

type RefreshSessionResponseDTO = {
	access: string;
	refresh: string;
}

export class RefreshSessionTranslator {
	public toRefreshSessionResponseDTO(tokens: TokenPair): RefreshSessionResponseDTO {
		return {
			access: tokens.get(TokenType.ACCESS)?.stringifiedToken || '',
			refresh:  tokens.get(TokenType.REFRESH)?.stringifiedToken || '',
		};
	}
}
